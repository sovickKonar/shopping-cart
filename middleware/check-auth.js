export default function(ctx) {

	var cookies = null;
	if(process.server){
		cookies = ctx.req.headers.cookie;
	}
	ctx.store.dispatch('initAuth',cookies)
	.then(state=>{
		console.log('state is :',state)
		if(state === false)
			ctx.redirect('/signin');
	})

}
