const Cookies = require( 'js-cookie');

export default function (ctx){
	if(!ctx.store.getters.getToken){
		ctx.redirect('/signin');
	}

}