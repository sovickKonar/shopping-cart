import Vuex from "vuex";
import Cookies from "js-cookie";

const createStore = () => {
	return new Vuex.Store({
		state: {
			products: [
				{
					id: "1",
					name: "product 1",
					price: "20",
				},
				{
					id: "2",
					name: "product 2",
					price: "22",
				},
				{
					id: "3",
					name: "product 3",
					price: "120",
				},
				{
					id: "4",
					name: "product 4",
					price: "450",
				},
				{
					id: "5",
					name: "product 5",
					price: "83",
				},
				{
					id: "6",
					name: "product 5",
					price: "23",
				},
			],
			cart: [],
			total: 0,
			isLoggedIn: false,
			token:null ,
			deliveryDetails:{}
		},
		mutations: {
			addtoCart(state, payload) {
				const items = state.cart;
				let isExisting = items.find((item) => item.id === payload.id);
				if (isExisting) {
					for (let item of items) {
						if (item.id == payload.id) {
							item.quantity++;
							state.total += parseInt(item.price);
						}
					}
				} else {
					items.push({
						id: payload.id,
						name: payload.name,
						price: payload.price,
						quantity: 1,
					});
					state.total += parseInt(payload.price);
				}
			},
			increaseQuantity(state, id) {
				const items = state.cart;
				for (let item of items) {
					if (item.id == id) {
						item.quantity++;
						state.total += parseInt(item.price);
					}
				}
			},
			decreaseQuantity(state, id) {
				const items = state.cart;

				for (let i in items) {
					if (items[i].id == id) {
						if (items[i].quantity >= 1) {
							items[i].quantity--;
							state.total -= parseInt(items[i].price);

							if (items[i].quantity == 0) {
								console.log("should be removed");
								items.splice(i, 1);
							}
						}
					}
				}
			},
			setLoginState(state, payload) {
				state.isLoggedIn = payload;
			},
			loginUser(state, token) {
				state.token = token;
				localStorage.setItem("token", token);
				Cookies.set("token", token);
			},
			setDelivery(state,payload){
				state.deliveryDetails = payload;
			}
		},
		getters: {
			getProducts(store) {
				return store.products;
			},
			getCartItems(store) {
				return store.cart;
			},
			getTotal(state) {
				return state.total;
			},
			getLoginState(state) {
				return state.isLoggedIn;
			},
			getToken(state) {
				return state.token != null;
			},
			getDelivery(state){
				return state.deliveryDetails;
			}
		},
		actions: {
			addItem(context, payload) {
				context.commit("addtoCart", payload);
			},
			increaseItem(ctx, payload) {
				ctx.commit("increaseQuantity", payload);
			},
			decreaseItem(ctx, payload) {
				ctx.commit("decreaseQuantity", payload);
			},
			loginUser(ctx, user) {
				let url =
					"https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAEvJJM-LImCyL1QhHNe7lbg3HCkcuFuEw";
				this.$axios
					.post(url, user)
					.then((result) => {
						console.log(result.data.idToken);
						ctx.commit("loginUser", result.data.idToken);
						ctx.commit("setLoginState", true);
						// save the token in localstorage
						localStorage.setItem("token", result.data.idToken);
						this.$router.push('/products');
					})
					.catch((err) => {
						console.log(err.message);
					});
			},
			initAuth(ctx, cookie) {
				if (cookie) {
					// console.log(cookie);
					console.log('running on server');
					let extractToken = cookie.split(';').find(el=> el.trim().startsWith('token='));
					if(!extractToken)
						return false;
					let token = extractToken.split('=')[1]
					// console.log('cookie token :',token);
					if(token){
						ctx.commit("setLoginState", true);
						return true
						
					}
					else
						return false

				} else {
					console.log('running on client');
					const token = localStorage.getItem("token");
					if (!token) return false;
					else{
						ctx.commit("setLoginState", true);
						return true;
					} 	
				}
			},
			setStateHeader(ctx){
				ctx.commit('setLoginState',false);
			},
			setDelivery(ctx,payload){
				ctx.commit('setDelivery',payload);
			},
			invoiceCheck(){
				console.log('invoice checkked invoked');

			}

		},
	});
};

export default createStore;
